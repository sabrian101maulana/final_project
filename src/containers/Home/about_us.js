import React from 'react'
import { View, Text, Image, TouchableOpacity, StyleSheet, ScrollView } from 'react-native'

const Dashboard = ({ navigation }) => {
    return (
        <View style={styles.container}>
            <View style={styles.header}>
                <TouchableOpacity onPress={() => navigation.goBack()}>
                    <Image source={{ uri: 'https://i.ibb.co/M76ZYhZ/left-arrow.png' }} style={styles.back} />
                </TouchableOpacity>
                <Text style={styles.about}>About US</Text>
            </View>
            <ScrollView>
                <View style={styles.container}>
                    <View style={styles.containerTitle}>
                        <Text style={styles.title}>Our Company</Text>
                    </View>
                    <View style={styles.content}>
                        <Text style={styles.txtContent}>Carsale  is the largest online automotive, motorcycle and marine classifieds business. Attracting more peoples interested in buying or selling cars, motorcycles, trucks, caravans and boats than any other classified group of websites. Carsale develops world leading technology and advertising solutions that drive its business around the world.</Text>
                    </View>
                    <View style={styles.containerTitle}>
                        <Text style={styles.title}>The Carsale Vision</Text>
                    </View>
                    <View style={styles.content}>
                        <Text style={styles.txtContent}>Our vision is to provide a smooth car buying and selling journey for all Countries. At carsale, everything we do from product development to marketing & communications aims to empower our customers and give them greater confidence when it comes to trading cars – regardless of how much they know about cars or how engaged they might be in the process.</Text>
                    </View>
                    <View style={styles.containerTitle}>
                        <Text style={styles.title}>Our Products & Services</Text>
                    </View>
                    <View style={styles.content}>
                        <Text style={styles.txtContent}>Our product portfolio continues to expand and diversify, allowing us to grow with the evolving needs of our customers and provide support during every step of their journey.</Text>
                    </View>
                    <View style={styles.containerTitle}>
                        <Text style={styles.title}>Advertise with us</Text>
                    </View>
                    <View style={styles.content}>
                        <Text style={styles.txtContent}>We provide online advertising solutions to media agencies and their clients, dealers (in all industries), industry organisations.</Text>
                        <View style={{ marginLeft: 5 }}>
                            <View style={{ height: 30, flexDirection: 'row', margin: 5, alignItems: 'center' }}>
                                <Image source={{ uri: 'https://i.ibb.co/MVsJXwh/Vector.png' }} style={{ height: 25, width: 25 }} />
                                <Text style={{ marginLeft: 5 }}>+61 3 9093 8600</Text>
                            </View>
                            <View style={{ height: 30, flexDirection: 'row', margin: 5, alignItems: 'center' }}>
                                <Image source={{ uri: 'https://i.ibb.co/CsK1Fzq/email.png' }} style={{ height: 25, width: 25 }} />
                                <Text style={{ marginLeft: 5 }}>carsale@gmail.com</Text>
                            </View>
                        </View>
                    </View>
                </View>
            </ScrollView>
        </View>
    )
}

export default Dashboard

const styles = StyleSheet.create({
    container: {
        flex: 1
    },
    header: {
        height: 40,
        alignItems: 'center',
        flexDirection: 'row',
        backgroundColor: '#5c5e62'
    },
    back: {
        height: 30,
        width: 25,
        marginRight: 5,
        marginLeft: 5
    },
    about: {
        color: 'white',
        fontWeight: 'bold',
        fontSize: 16
    },
    containerTitle: {
        height: 30,
        margin: 5,
        marginBottom: 0
    },
    title: {
        fontSize: 20,
        fontWeight: 'bold'
    },
    content: {
        marginLeft: 5,
        marginRight: 5
    },
    txtContent: {
        textAlign: 'justify'
    }
})