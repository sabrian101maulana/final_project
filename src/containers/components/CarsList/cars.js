export default [
  {
    name: 'Porsche 911',
    tagline: 'Starting at $98,42',
    image: require('../../../assets/images/ModelT.jpg'),
  },
  {
    name: 'Model S',
    tagline: 'Starting at $68,42',
    image: require('../../../assets/images/ModelS.jpeg'),
  },
  {
    name: 'Model 3',
    tagline: 'Starting at $76,21',
    image: require('../../../assets/images/Model3.jpeg'),
  },
  {
    name: 'Model X',
    tagline: 'Starting at $82,34',
    image: require('../../../assets/images/ModelX.jpeg'),
  },
  {
    name: 'Model Y',
    tagline: 'Starting at $59,63',
    image: require('../../../assets/images/ModelY.jpeg'),
  }
];
